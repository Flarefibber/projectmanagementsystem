package main.model;

public class Work_on_projects {
    private long  work_on_projects_id;
    private long  developer_id;
    private long  project_id;

    public long  getWork_on_projects_id() {
        return work_on_projects_id;
    }

    public void setWork_on_projects_id(long  work_on_projects_id) {
        this.work_on_projects_id = work_on_projects_id;
    }

    public long  getDeveloper_id() {
        return developer_id;
    }

    public void setDeveloper_id(long  developer_id) {
        this.developer_id = developer_id;
    }

    public long  getProject_id() {
        return project_id;
    }

    public void setProject_id(long  project_id) {
        this.project_id = project_id;
    }

    @Override
    public String toString() {
        return "Work_on_projects{" +
                "work_on_projects_id=" + work_on_projects_id +
                ", developer_id=" + developer_id +
                ", project_id=" + project_id +
                '}';
    }
}
