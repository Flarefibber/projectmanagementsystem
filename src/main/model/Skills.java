package main.model;

public class Skills {
    private long  skill_id;
    private String skill;

    public long  getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(long  skill_id) {
        this.skill_id = skill_id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "Skills{" +
                "skill_id=" + skill_id +
                ", skill='" + skill + '\'' +
                '}';
    }
}
