package main.model;

import java.sql.Date;

public class Projects {
    private long  project_id;
    private String name_project;
    private Date projec_start_date;
    private Date deadline;

    public long  getProject_id() {
        return project_id;
    }

    public void setProject_id(long  project_id) {
        this.project_id = project_id;
    }

    public String getName_project() {
        return name_project;
    }

    public void setName_project(String name_project) {
        this.name_project = name_project;
    }

    public Date getProjec_start_date() {
        return projec_start_date;
    }

    public void setProjec_start_date(Date projec_start_date) {
        this.projec_start_date = projec_start_date;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    @Override
    public String toString() {
        return "Projects{" +
                "project_id=" + project_id +
                ", name_project='" + name_project + '\'' +
                ", projec_start_date=" + projec_start_date +
                ", deadline=" + deadline +
                '}';
    }
}
