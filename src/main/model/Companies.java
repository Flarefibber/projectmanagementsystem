package main.model;

public class Companies {
    private long  company_id;
    private String name_company;

    public long  getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long  company_id) {
        this.company_id = company_id;
    }

    public String getName_company() {
        return name_company;
    }

    public void setName_company(String name_company) {
        this.name_company = name_company;
    }

    @Override
    public String toString() {
        return "Companies{" +
                "company_id=" + company_id +
                ", name_company='" + name_company + '\'' +
                '}';
    }
}
