package main.model;

public class Order {
    private long  order_id;
    private long  company_id;
    private long  project_id;
    private long  customer_id;

    public long  getOrder_id() {
        return order_id;
    }

    public void setOrder_id(long  order_id) {
        this.order_id = order_id;
    }

    public long  getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long  company_id) {
        this.company_id = company_id;
    }

    public long  getProject_id() {
        return project_id;
    }

    public void setProject_id(long  project_id) {
        this.project_id = project_id;
    }

    public long  getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long  customer_id) {
        this.customer_id = customer_id;
    }

    @Override
    public String toString() {
        return "Order{" +
                "order_id=" + order_id +
                ", company_id=" + company_id +
                ", project_id=" + project_id +
                ", customer_id=" + customer_id +
                '}';
    }
}
