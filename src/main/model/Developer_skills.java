package main.model;

public class Developer_skills {
    private long  developer_skills_id;
    private long  developer_id;
    private long  skill_id;

    public long  getDeveloper_skills_id() {
        return developer_skills_id;
    }

    public void setDeveloper_skills_id(long  developer_skills_id) {
        this.developer_skills_id = developer_skills_id;
    }

    public long  getDeveloper_id() {
        return developer_id;
    }

    public void setDeveloper_id(long  developer_id) {
        this.developer_id = developer_id;
    }

    public long  getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(long  skill_id) {
        this.skill_id = skill_id;
    }

    @Override
    public String toString() {
        return "Developer_skills{" +
                "developer_skills_id=" + developer_skills_id +
                ", developer_id=" + developer_id +
                ", skill_id=" + skill_id +
                '}';
    }
}
