package main.controller;

import main.dao.Developer_skillsDAO;
import main.model.Developer_skills;

import java.sql.SQLException;

public class Developer_skillsController extends ModelController<Developer_skills>{
    private final Developer_skillsDAO dao;
    public Developer_skillsController(Developer_skillsDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("DEVELOPER SKILLS MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL DEVELOPERS SKILLS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET DEVELOPER SKILLS BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW DEVELOPER SKILLS");
        System.out.println("---------------");
        System.out.println("Input id developer : ");
        Long id_developer =  SCANNER.nextLong();
        Developer_skills developer_skills = getNewInstance();
        developer_skills.setDeveloper_id(id_developer);
        System.out.println("Input id developer skill: ");
        Long skill = SCANNER.nextLong();
        developer_skills.setSkill_id(skill);
        this.dao.add(developer_skills);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE DEVELOPER SKILLS");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new id developer: ");
        Long id_developer = SCANNER.nextLong();
        Developer_skills developer_skills = getNewInstance();
        developer_skills.setDeveloper_id(id_developer);
        System.out.println("Input a new id developer skill: ");
        Long skill = SCANNER.nextLong();
        developer_skills.setSkill_id(skill);
        developer_skills.setDeveloper_id(id);
        this.dao.update(developer_skills);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE DEVELOPER SKILLS");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Developer_skills getNewInstance() {
        return new Developer_skills();
    }
}
