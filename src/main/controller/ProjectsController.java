package main.controller;

import main.dao.ProjectsDAO;
import main.model.Projects;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;

public final class ProjectsController extends ModelController<Projects>{
    private final ProjectsDAO dao;
    public ProjectsController(ProjectsDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("PROJECT MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL PROJECTS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET PROJECT BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException, IOException, ParseException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW PROJECT");
        System.out.println("---------------");
        System.out.println("Input name project: ");
        String name = SCANNER.nextLine();
        Projects projects = getNewInstance();
        projects.setName_project(name);
        System.out.println("Input start date project (dd.MM.yyyy): ");
        java.sql.Date start_date = (java.sql.Date) sdf.parse(SCANNERDATE.readLine());
        projects.setProjec_start_date(start_date);
        System.out.println("Input patronymic project (dd.MM.yyyy): ");
        java.sql.Date deadline = (java.sql.Date) sdf.parse(SCANNERDATE.readLine());
        projects.setDeadline(deadline);
        this.dao.add(projects);
    }

    @Override
    protected void updateById() throws SQLException, IOException, ParseException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE PROJECT");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new name project: ");
        String name = SCANNER.nextLine();
        Projects projects = getNewInstance();
        projects.setName_project(name);
        System.out.println("Input a new start date project (dd.MM.yyyy): ");
        java.sql.Date start_date = (java.sql.Date) sdf.parse(SCANNERDATE.readLine());
        projects.setProjec_start_date(start_date);
        System.out.println("Input a new patronymic project (dd.MM.yyyy): ");
        java.sql.Date deadline = (java.sql.Date) sdf.parse(SCANNERDATE.readLine());
        projects.setDeadline(deadline);
        projects.setProject_id(id);
        this.dao.update(projects);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE PROJECT");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Projects getNewInstance() {
        return new Projects();
    }
}
