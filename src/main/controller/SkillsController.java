package main.controller;

import main.dao.SkillsDAO;
import main.model.Skills;

import java.sql.SQLException;

public final class SkillsController extends ModelController<Skills>{
    private final SkillsDAO dao;
    public SkillsController(SkillsDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("SKILL MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL SKILLS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET SKILL BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW SKILL");
        System.out.println("---------------");
        System.out.println("Input skill: ");
        String skill = SCANNER.nextLine();
        Skills skills = getNewInstance();
        skills.setSkill(skill);
        this.dao.add(skills);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE SKILL");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new skill: ");
        String skill = SCANNER.nextLine();
        Skills skills = getNewInstance();
        skills.setSkill(skill);
        skills.setSkill_id(id);
        this.dao.update(skills);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE SKILL");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Skills getNewInstance() {
        return new Skills();
    }
}
