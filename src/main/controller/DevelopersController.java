package main.controller;

import main.dao.DevelopersDAO;
import main.model.Developers;

import java.sql.SQLException;

public final class DevelopersController extends ModelController<Developers>{
    private final DevelopersDAO dao;
    public DevelopersController(DevelopersDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("DEVELOPER MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL DEVELOPERS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET DEVELOPER BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW DEVELOPER");
        System.out.println("---------------");
        System.out.println("Input name developer: ");
        String name = SCANNER.nextLine();
        Developers developers = getNewInstance();
        developers.setName(name);
        System.out.println("Input surname developer: ");
        String surname = SCANNER.nextLine();
        developers.setSurname(surname);
        System.out.println("Input patronymic developer: ");
        String patronymic = SCANNER.nextLine();
        developers.setPatronymic(patronymic);
        this.dao.add(developers);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE DEVELOPER");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new name developer: ");
        String name = SCANNER.nextLine();
        Developers developers = getNewInstance();
        developers.setName(name);
        System.out.println("Input a new surname developer: ");
        String surname = SCANNER.nextLine();
        developers.setSurname(surname);
        System.out.println("Input a new patronymic developer: ");
        String patronymic = SCANNER.nextLine();
        developers.setPatronymic(patronymic);
        developers.setDeveloper_id(id);
        this.dao.update(developers);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE DEVELOPER");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Developers getNewInstance() {
        return new Developers();
    }

}
