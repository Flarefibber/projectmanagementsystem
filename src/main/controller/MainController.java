package main.controller;

import main.fabrica.FactoryConnection;
import main.fabrica.FactoryController;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public final class MainController extends AbstractController {
    private final CompaniesController companiesController;

    private final CustomersController customersController;

    private final DevelopersController developersController;

    private final ProjectsController projectsController;

    private final SkillsController skillsController;

    private final Developer_skillsController developer_skillsController;

    private final OrderController orderController;

    private final Work_on_projectsController work_on_projectsController;

    public MainController() throws SQLException {
        this.companiesController = FactoryController.getCompanyController();
        this.customersController = FactoryController.getCustomerController();
        this.developersController = FactoryController.getDeveloperController();
        this.projectsController = FactoryController.getProjectController();
        this.skillsController = FactoryController.getSkillController();
        this.developer_skillsController = FactoryController.getDeveloper_skillsController();
        this.orderController = FactoryController.getOrderController();
        this.work_on_projectsController = FactoryController.getWork_on_projectsController();
    }

    @Override
    protected void choiceOfMenu(int menuNumber) throws SQLException, IOException, ParseException {
        switch (menuNumber) {
            case 1:
                this.companiesController.startMenu();
                break;
            case 2:
                this.customersController.startMenu();
                break;
            case 3:
                this.developersController.startMenu();
                break;
            case 4:
                this.projectsController.startMenu();
                break;
            case 5:
                this.skillsController.startMenu();
                break;
            case 6:
                this.developer_skillsController.startMenu();
                break;
            case 7:
                this.orderController.startMenu();
                break;
            case 8:
                this.work_on_projectsController.startMenu();
                break;
            default:
                FactoryConnection.getConnectionJdbs().close();
                System.exit(0);
        }
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("MAIN MENU");
        System.out.println("---------------");
        System.out.println("1 - company menu");
        System.out.println("2 - customer menu");
        System.out.println("3 - developer menu");
        System.out.println("4 - project menu");
        System.out.println("5 - skill menu");
        System.out.println("6 - developer skill menu");
        System.out.println("7 - order menu");
        System.out.println("8 - work on projects menu");
        System.out.println("0 - exit");
    }
}
