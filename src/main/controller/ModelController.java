package main.controller;

import main.dao.GenericDAO;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public abstract class ModelController<T> extends AbstractController {
    private final GenericDAO<T> dao;

    public ModelController(GenericDAO<T> dao) {
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("1 - get all");
        System.out.println("2 - get by id");
        System.out.println("3 - add");
        System.out.println("4 - update");
        System.out.println("5 - remove");
        System.out.println("0 - in main menu");
    }

    @Override
    protected void choiceOfMenu(int menuNumber) throws SQLException, IOException, ParseException {
        switch (menuNumber) {
            case 1:
                getAllAndPrint();
                break;
            case 2:
                getByIdAndPrint();
                break;
            case 3:
                addNew();
                break;
            case 4:
                updateById();
                break;
            case 5:
                removeById();
                break;
        }
    }

    protected void getAllAndPrint() throws SQLException {
        this.dao.getAll().forEach(System.out::println);
    }

    protected void getByIdAndPrint() throws SQLException {
        System.out.println("Input id:\n");
        long id = SCANNER.nextLong();
        System.out.println(this.dao.get(id));
        SCANNER.nextLine();
    }

    protected abstract void addNew() throws SQLException, IOException, ParseException;

    protected abstract void updateById() throws SQLException, IOException, ParseException;

    protected void removeById() throws SQLException {
        System.out.println("Input id:\n");
        long id = SCANNER.nextLong();
        this.dao.remove(id);
        SCANNER.nextLine();
    }

    protected abstract T getNewInstance();
}
