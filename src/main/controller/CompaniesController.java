package main.controller;

import main.dao.CompaniesDAO;
import main.model.Companies;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

public final class CompaniesController extends ModelController<Companies>{
    private final CompaniesDAO dao;

    public CompaniesController(CompaniesDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("COMPANY MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL COMPANY");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET COMPANY BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW COMPANY");
        System.out.println("---------------");
        System.out.println("Input name company: ");
        String name = SCANNER.nextLine();
        Companies companies = getNewInstance();
        companies.setName_company(name);
        this.dao.add(companies);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE COMPANY");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new name company: ");
        String name = SCANNER.nextLine();
        Companies companies = getNewInstance();
        companies.setName_company(name);
        companies.setCompany_id(id);
        this.dao.update(companies);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE COMPANY");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Companies getNewInstance() {
        return new Companies();
    }
}
