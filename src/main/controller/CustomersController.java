package main.controller;

import main.dao.CustomersDAO;
import main.model.Customers;

import java.sql.SQLException;

public final class CustomersController extends ModelController<Customers>{
    private final CustomersDAO dao;
    public CustomersController(CustomersDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("CUSTOMER MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL CUSTOMERS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET CUSTOMER BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW CUSTOMER");
        System.out.println("---------------");
        System.out.println("Input name customer: ");
        String name = SCANNER.nextLine();
        Customers customers = getNewInstance();
        customers.setName_customer(name);
        this.dao.add(customers);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE CUSTOMER");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new name customer: ");
        String name = SCANNER.nextLine();
        Customers customers = getNewInstance();
        customers.setName_customer(name);
        customers.setCustomer_id(id);
        this.dao.update(customers);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE CUSTOMER");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Customers getNewInstance() {
        return new Customers();
    }
}
