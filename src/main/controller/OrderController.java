package main.controller;

import main.dao.OrderDAO;
import main.model.Order;

import java.sql.SQLException;

public class OrderController extends ModelController<Order>{
    private final OrderDAO dao;
    public OrderController(OrderDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("ORDER MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL ORDER");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ORDER BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW ORDER");
        System.out.println("---------------");
        System.out.println("Input id company : ");
        Long company =  SCANNER.nextLong();
        Order order = getNewInstance();
        order.setCompany_id(company);
        System.out.println("Input id project: ");
        Long skill = SCANNER.nextLong();
        order.setProject_id(skill);
        System.out.println("Input id customer: ");
        Long customer = SCANNER.nextLong();
        order.setCustomer_id(customer);
        this.dao.add(order);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE ORDER");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new id company: ");
        Long company = SCANNER.nextLong();
        Order order = getNewInstance();
        order.setCompany_id(company);
        System.out.println("Input a new id project: ");
        Long project = SCANNER.nextLong();
        order.setProject_id(project);
        System.out.println("Input a new id customer: ");
        Long customer = SCANNER.nextLong();
        order.setCustomer_id(customer);
        order.setOrder_id(id);
        this.dao.update(order);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE ORDER");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Order getNewInstance() {
        return new Order();
    }
}
