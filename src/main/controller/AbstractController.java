package main.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public abstract  class AbstractController {
    protected static final Scanner SCANNER = new Scanner(System.in);
    protected static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
    protected static final BufferedReader SCANNERDATE = new BufferedReader(new InputStreamReader(System.in));

    public void startMenu() throws SQLException, IOException, ParseException {
        while (true) {
            printMainMenu();
            int menuNumber = inputChoice();
            if (menuNumber == 0) {
                break;
            }
            choiceOfMenu(menuNumber);
        }
    }

    protected static int inputChoice() {
        System.out.println("Input:\n");
        int result;
        try {
            result = Integer.parseInt(SCANNER.nextLine());
        } catch (NumberFormatException ex) {
            result = -1;
        }
        return result;
    }

    protected abstract void printMainMenu();

    protected abstract void choiceOfMenu(int menuNumber) throws SQLException, IOException, ParseException;
}
