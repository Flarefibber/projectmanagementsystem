package main.controller;

import main.dao.Work_on_projectsDAO;
import main.model.Work_on_projects;

import java.sql.SQLException;

public class Work_on_projectsController extends ModelController<Work_on_projects>{
    private final Work_on_projectsDAO dao;
    public Work_on_projectsController(Work_on_projectsDAO dao) {
        super(dao);
        this.dao = dao;
    }

    @Override
    protected void printMainMenu() {
        System.out.println("===============");
        System.out.println("WORK ON PROJECTS MENU");
        System.out.println("---------------");
        super.printMainMenu();
    }

    @Override
    protected void getAllAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET ALL WORK ON PROJECTS");
        System.out.println("---------------");
        super.getAllAndPrint();
    }

    @Override
    protected void getByIdAndPrint() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("GET WORK ON PROJECTS BY ID");
        System.out.println("---------------");
        super.getByIdAndPrint();
    }

    @Override
    protected void addNew() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("ADD NEW WORK ON PROJECTS");
        System.out.println("---------------");
        System.out.println("Input id developer: ");
        Long id_developer =  SCANNER.nextLong();
        Work_on_projects work_on_projects = getNewInstance();
        work_on_projects.setDeveloper_id(id_developer);
        System.out.println("Input id project: ");
        Long project_id = SCANNER.nextLong();
        work_on_projects.setProject_id(project_id);
        this.dao.add(work_on_projects);
    }

    @Override
    protected void updateById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("UPDATE WORK ON PROJECTS");
        System.out.println("---------------");
        System.out.println("Input id: ");
        long id = SCANNER.nextLong();
        SCANNER.nextLine();
        System.out.println("Input a new id developer: ");
        Long id_developer = SCANNER.nextLong();
        Work_on_projects work_on_projects = getNewInstance();
        work_on_projects.setDeveloper_id(id_developer);
        System.out.println("Input a new id project: ");
        Long project_id = SCANNER.nextLong();
        work_on_projects.setProject_id(project_id);
        work_on_projects.setDeveloper_id(id);
        this.dao.update(work_on_projects);
    }

    @Override
    protected void removeById() throws SQLException {
        System.out.println("^^^^^^^^^^^^^^^");
        System.out.println("REMOVE WORK ON PROJECTS");
        System.out.println("---------------");
        super.removeById();
    }

    @Override
    protected Work_on_projects getNewInstance() {
        return new Work_on_projects();
    }
}
