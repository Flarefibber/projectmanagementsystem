package main.fabrica;

import main.dao.*;
import main.dao.impl.*;

import java.sql.SQLException;

public final class FactoryDao {
    private static CompaniesDAO companiesDAO;

    private static CustomersDAO customersDAO;

    private static DevelopersDAO developersDAO;

    private static ProjectsDAO projectsDAO;

    private static SkillsDAO skillsDAO;

    private static Developer_skillsDAO developer_skillsDAO;

    private static OrderDAO orderDAO;

    private static Work_on_projectsDAO work_on_projectsDAO;

    private FactoryDao() {

    }

    public static CompaniesDAO getCompaniesDAO() throws SQLException {
        if (companiesDAO == null) {
            initCompaniesDAO();
        }
        return companiesDAO;
    }

    public static CustomersDAO getCustomersDAO() throws SQLException {
        if (customersDAO == null) {
            initCustomersDAO();
        }
        return customersDAO;
    }

    public static DevelopersDAO getDevelopersDAO() throws SQLException {
        if (developersDAO == null) {
            initDevelopersDAO();
        }
        return developersDAO;
    }

    public static ProjectsDAO getProjectsDAO() throws SQLException {
        if (projectsDAO == null) {
            initProjectsDAO();
        }
        return projectsDAO;
    }

    public static SkillsDAO getSkillsDAO() throws SQLException {
        if (skillsDAO == null) {
            initSkillsDAO();
        }
        return skillsDAO;
    }

    public static Developer_skillsDAO getDeveloper_skillsDAO() throws SQLException {
        if (developer_skillsDAO == null) {
            initDeveloper_skillsDAO();
        }
        return developer_skillsDAO;
    }

    public static OrderDAO getOrderDAO() throws SQLException {
        if (orderDAO == null) {
            initOrderDAO();
        }
        return orderDAO;
    }

    public static Work_on_projectsDAO getWork_on_projectsDAO() throws SQLException {
        if (work_on_projectsDAO == null) {
            initWork_on_projectsDAO();
        }
        return work_on_projectsDAO;
    }

    private static void initCompaniesDAO() throws SQLException {
        companiesDAO = new CompaniesDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initCustomersDAO() throws SQLException {
        customersDAO = new CustomersDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initDevelopersDAO() throws SQLException {
        developersDAO = new DevelopersDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initProjectsDAO() throws SQLException {
        projectsDAO = new ProjectsDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initSkillsDAO() throws SQLException {
        skillsDAO = new SkillsDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initDeveloper_skillsDAO() throws SQLException {
        developer_skillsDAO = new Developer_skillsDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initOrderDAO() throws SQLException {
        orderDAO = new OrderDAOImpl(FactoryConnection.getConnectionJdbs());
    }

    private static void initWork_on_projectsDAO() throws SQLException {
        work_on_projectsDAO = new Work_on_projectsDAOImpl(FactoryConnection.getConnectionJdbs());
    }
}
