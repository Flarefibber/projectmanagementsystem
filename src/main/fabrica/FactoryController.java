package main.fabrica;

import main.controller.*;

import java.sql.SQLException;

public final class FactoryController {

    private static CompaniesController companiesController;

    private static CustomersController customersController;

    private static DevelopersController developersController;

    private static ProjectsController projectsController;

    private static SkillsController skillsController;

    private static Developer_skillsController developer_skillsController;

    private static OrderController orderController;

    private static Work_on_projectsController work_on_projectsController;

    private FactoryController() {

    }

    public static CompaniesController getCompanyController() throws SQLException {
        if (companiesController == null) {
            initCompaniesController();
        }
        return companiesController;
    }

    public static CustomersController getCustomerController() throws SQLException {
        if (customersController == null) {
            initCustomerController();
        }
        return customersController;
    }

    public static DevelopersController getDeveloperController() throws SQLException {
        if (developersController == null) {
            initDeveloperController();
        }
        return developersController;
    }

    public static ProjectsController getProjectController() throws SQLException {
        if (projectsController == null) {
            initProjectController();
        }
        return projectsController;
    }

    public static SkillsController getSkillController() throws SQLException {
        if (skillsController == null) {
            initSkillController();
        }
        return skillsController;
    }

    public static Developer_skillsController getDeveloper_skillsController() throws SQLException {
        if (developer_skillsController == null) {
            initDeveloper_skillsController();
        }
        return developer_skillsController;
    }

    public static OrderController getOrderController() throws SQLException {
        if (orderController == null) {
            initOrderController();
        }
        return orderController;
    }

    public static Work_on_projectsController getWork_on_projectsController() throws SQLException {
        if (work_on_projectsController == null) {
            initWork_on_projectsController();
        }
        return work_on_projectsController;
    }

    private static void initCompaniesController() throws SQLException {
        companiesController = new CompaniesController(FactoryDao.getCompaniesDAO());
    }

    private static void initCustomerController() throws SQLException {
        customersController = new CustomersController(FactoryDao.getCustomersDAO());
    }

    private static void initDeveloperController() throws SQLException {
        developersController = new DevelopersController(FactoryDao.getDevelopersDAO());
    }

    private static void initProjectController() throws SQLException {
        projectsController = new ProjectsController(FactoryDao.getProjectsDAO());
    }

    private static void initSkillController() throws SQLException {
        skillsController = new SkillsController(FactoryDao.getSkillsDAO());
    }

    private static void initDeveloper_skillsController() throws SQLException {
        developer_skillsController = new Developer_skillsController(FactoryDao.getDeveloper_skillsDAO());
    }

    private static void initOrderController() throws SQLException {
        orderController = new OrderController(FactoryDao.getOrderDAO());
    }

    private static void initWork_on_projectsController() throws SQLException {
        work_on_projectsController = new Work_on_projectsController(FactoryDao.getWork_on_projectsDAO());
    }
}
