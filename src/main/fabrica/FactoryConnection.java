package main.fabrica;

import main.connection.ConnectionDB;
import main.connection.ConnectionJdbs;

public final class FactoryConnection {
    private final static String DEFAULT_DATABASE_NAME = "db_developers";

    private final static String DEFAULT_USERNAME = "root";

    private final static String DEFAULT_PASSWORD = "root";

    private static ConnectionJdbs connectionJdbs;

    private FactoryConnection() {

    }

    public static ConnectionDB getConnectionJdbs() {
        if (connectionJdbs == null) {
            initConnectionJdbs();
        }
        return connectionJdbs;
    }

    private static void initConnectionJdbs() {
        connectionJdbs = new ConnectionJdbs(
                DEFAULT_DATABASE_NAME,
                DEFAULT_USERNAME,
                DEFAULT_PASSWORD
        );
    }
}
