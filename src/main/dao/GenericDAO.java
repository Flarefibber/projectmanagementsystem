package main.dao;

import java.sql.SQLException;
import java.util.Collection;

public interface GenericDAO<T> {
    void add(T entity) throws SQLException;

    void update(T entity) throws SQLException;

    T get(long  id) throws SQLException;

    void remove(T entity) throws SQLException;

    void remove(long  id) throws SQLException;

    Collection<T> getAll() throws SQLException;
}
