package main.dao;

import main.model.Developers;

public interface DevelopersDAO extends GenericDAO<Developers> {
}
