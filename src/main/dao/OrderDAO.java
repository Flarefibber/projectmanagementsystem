package main.dao;

import main.model.Order;

public interface OrderDAO extends GenericDAO<Order> {
}